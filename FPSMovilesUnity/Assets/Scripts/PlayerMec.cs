﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerMec : MonoBehaviour {

    public int hp = 100;
    float cooldownDamage = 0;
    [SerializeField]
    private AudioClip hurtSound;
    [SerializeField]
    private AudioSource sound;
    string scene;

    // Use this for initialization
    void Start () {
        scene = SceneManager.GetActiveScene().name;
	}
	
	// Update is called once per frame
	void Update () {
        cooldownDamage += Time.deltaTime;
        if (Input.GetKeyDown(KeyCode.T))
            Die();
    }

    public void TakeDamage()
    {
        
        if (cooldownDamage >= 0)
        {
            hp -= 35;
            cooldownDamage = -3;
            sound.PlayOneShot(hurtSound);
        }

        if (hp <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        SceneManager.LoadScene("MainMenu");        
    }
}
