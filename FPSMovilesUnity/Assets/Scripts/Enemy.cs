﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public int enemyType;

    public float range = 2f;
    public float health = 100f;
    //public static int count = 0;
    public Animator anim;
    public float fwdSpeed;
    public GameObject player;
    public GameObject projectile;
    public GameObject head;
    public AudioSource audio;
    public AudioClip ughAudio;
    public AudioClip attackAudio;
    public AudioClip painAudio;
    public float timeTest;
    public float cooldown=0;
    public float startWaitTime;
    public float stoppingDistance;
    public float startTimeBtwShots;
    public float retreatDistance;
    ObjectPooler objectPooler;

    public RoomCheck roomCheck;
    public Transform[] moveSpots;


    public int layerMask = 1<<11;

    //public float time;
    private float timeBtwShots;
    private Vector3 offset;
    private bool isChasing = false;
    private bool dead = false;
    private bool attacking = false;
    private bool takingDmg = false;
    private float waitTime;
    private int randomSpot;
    private bool isMoving;


    private void Awake()
    {
        //count++;
        objectPooler = ObjectPooler.Instance;
        anim = GetComponent<Animator>();
        audio = GetComponent<AudioSource>();
        player = GameObject.FindWithTag("Player");
       // layerMask = ~layerMask;

        offset.y = 1;

        if(enemyType == 1)
        {
            randomSpot = Random.Range(0, moveSpots.Length);
        }
    }

    private void Update()   //Podria haber usado herencias. Para la proxima
    {
        if (enemyType == 0)
        {
            var lookPos = player.transform.position - transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            if (!dead)
            {
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 100);
            }
            if (isChasing && !dead && !takingDmg && !attacking)
            {
                transform.Translate(0, 0, fwdSpeed * Time.deltaTime);
                anim.Play("walk");
            }

            RaycastHit hit;
            if (Physics.Raycast(gameObject.transform.position + offset, gameObject.transform.forward, out hit, range, layerMask))
            {
                //Debug.Log(hit.transform.tag);
                //Debug.DrawRay(transform.position+offset, transform.forward*hit.distance, Color.green);
                if (hit.transform.tag == "Player" && cooldown <= 0 && !dead)
                {
                    cooldown = 3;
                    StartCoroutine(Animate(1.3f, "jumpBiteAttack_RM"));
                }
            }
            cooldown -= Time.deltaTime;

        }

        if (enemyType == 1)
        {
            if (!dead)
            {
                transform.position = Vector3.MoveTowards(transform.position, moveSpots[randomSpot].position, fwdSpeed * Time.deltaTime);
                var lookPos = moveSpots[randomSpot].transform.position - transform.position;
                lookPos.y = 0;

                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 100);
                if (isMoving == true)
                {
                    StartCoroutine(Animate(1.0f, "walk"));
                }
            }

            if (Vector3.Distance(transform.position, moveSpots[randomSpot].position) < 0.2f)
            {
                if (waitTime <= 0)
                {
                    randomSpot = Random.Range(0, moveSpots.Length);
                    waitTime = startWaitTime;
                    isMoving = true;
                }
                else
                {
                    isMoving = false;
                    waitTime -= Time.deltaTime;
                }
            }
        }


        if (enemyType == 2)
        {
            if(!dead)
            {
                
                var lookPos = player.transform.position - transform.position;
                lookPos.y = 0;

                var rotation = Quaternion.LookRotation(lookPos);
                transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * 100);

                var playerPos = player.transform.position;
                playerPos.y = 0;

                if (Vector3.Distance(transform.position, playerPos) > stoppingDistance)
                {
                    transform.position = Vector3.MoveTowards(transform.position, playerPos, fwdSpeed * Time.deltaTime);
                    
                }
                else if (Vector3.Distance(transform.position, playerPos) < stoppingDistance
                    && Vector3.Distance(transform.position, playerPos) > retreatDistance)
                {
                    transform.position = this.transform.position;
                    
                }
                else if (Vector3.Distance(transform.position, playerPos) < retreatDistance)
                {
                    transform.position = Vector3.MoveTowards(transform.position, playerPos, -fwdSpeed * Time.deltaTime);
                    
                }

                if (isMoving == true && !dead && !takingDmg && !attacking)
                {
                    anim.Play("walk");
                }

                if (timeBtwShots <= 0)
                {
                    StartCoroutine(Animate(0.6f, "attack"));
                    ObjectPooler.Instance.SpawnFromPool("Projectile", head.transform.position, Quaternion.identity);
                    //Instantiate(projectile, head.transform.position, Quaternion.identity);
                    timeBtwShots = startTimeBtwShots;
                }
                else
                {
                    timeBtwShots -= Time.deltaTime;
                }
            }            
        }
    }

    public void TakeDamage(float dmg)
    {
        if (!dead)
        {
            isChasing = true;
            health -= dmg;

            if (health <= 0f)
            {
                roomCheck.EnemyDown();
                Die();
            }
            else
            {
                if (enemyType == 0) 
                    StartCoroutine(Animate(1.0f, "getHit2")); 
                else if(enemyType == 1)
                    audio.PlayOneShot(ughAudio);

            }
        }
    }
    
    IEnumerator Animate(float _time, string animName)
    {
        if (animName == "death")
        {
            dead = true;
            anim.Play(animName);
            audio.PlayOneShot(painAudio);
            yield return new WaitForSeconds(_time);
            gameObject.SetActive(false);
        }
        if(animName == "getHit2")
        {
            
            takingDmg = true;
            anim.Play("getHit2");
            audio.PlayOneShot(ughAudio);
            yield return new WaitForSeconds(_time);
            takingDmg = false;
        }
        if(animName == "walk")
        {
            anim.Play(animName);
            yield return new WaitForSeconds(_time);
        }
        if (animName == "attack")
        {
            isMoving = false;
            attacking = true;
            anim.Play(animName);
            audio.PlayOneShot(attackAudio);

            yield return new WaitForSeconds(_time);
            
            isMoving = true;

            attacking = false;
        }
    }

    void Die()
    {
        StartCoroutine(Animate(5, "death"));
    }

    public bool GetDamage()
    {
        if (!dead)
            return false;
        else
            return true;
    }

}
