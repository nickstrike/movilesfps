﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[AddComponentMenu("Camera-Control/CameraMovement")] 

public class CameraMovement : MonoBehaviour
{
    public Joystick joystick;
    
    public enum RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
    [SerializeField]
    public RotationAxes axes = RotationAxes.MouseXAndY;
    [SerializeField]
    public float sensitivityX;
    [SerializeField]
    public float sensitivityY;
    [SerializeField]
    float minX = -360F;
    [SerializeField]
    float maxX = 360F;
    [SerializeField]
    public float minY;
    [SerializeField]
    public float maxY;
    float RotationAxisY = 0F;
    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID
        maxY = 0;
#endif
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale != 0.0f)
        {
#if !UNITY_ANDROID
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            float RotationAxisX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

            RotationAxisY += Input.GetAxis("Mouse Y") * sensitivityY;
#else
            joystick.gameObject.SetActive(true);
            float RotationAxisX = transform.localEulerAngles.y + joystick.Horizontal * sensitivityX;

            RotationAxisY += joystick.Vertical * sensitivityY;
#endif
            RotationAxisY = Mathf.Clamp(RotationAxisY, minY, maxY); 

            transform.localEulerAngles = new Vector3(-RotationAxisY, RotationAxisX, 0);
            
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}
