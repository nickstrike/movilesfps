﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour, IPooledObject {

    public float speed;

    private Transform player;
    private Vector3 target;

	// Use this for initialization
	public void OnObjectSpawn () {
        player = GameObject.FindWithTag("Player").transform;

        target = new Vector3(player.position.x, player.position.y, player.position.z);		
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

        if (transform.position.x == target.x && transform.position.z == target.z)
            transform.position = new Vector3(-100, -100, -100);
	}

    /*void DestroyProjectile()
    {
        gameObject.SetActive(false);
    }*/
}
