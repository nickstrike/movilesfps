﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrol : MonoBehaviour {

    public float fwdSpeed;
    public float stoppingDistance;
    public float retreatDistance;

    public GameObject player;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");

    }

    private void Update()
    {
        if(Vector3.Distance(transform.position, player.transform.position) > stoppingDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, fwdSpeed * Time.deltaTime);
        }
        else if(Vector3.Distance(transform.position, player.transform.position) < stoppingDistance
            && Vector3.Distance(transform.position, player.transform.position) > retreatDistance)
        {
            transform.position = this.transform.position;
        }
        else if(Vector3.Distance(transform.position, player.transform.position) < retreatDistance)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, -fwdSpeed * Time.deltaTime);

        }
    }

}
