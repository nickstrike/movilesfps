﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portals : MonoBehaviour {

    private static int level = 1;
    [SerializeField]
    private bool startRoom = false;
    [SerializeField]
    private bool lastPortal = false;
    [SerializeField]
    private Transform roomToGo;
    [SerializeField]
    private Transform level1;
    [SerializeField]
    private Transform level2;
    [SerializeField]
    private Transform level3;
    [SerializeField]
    private Transform level4;
    [SerializeField]
    private Transform win;

    public void Start()
    {
        if(startRoom)
            level = 1;
    }

    public Transform Teleport()
    {
        if(startRoom)
        {
            if (level == 1)
            {
                roomToGo = level1;
                level++;
            }
            else if (level == 2)
            {
                roomToGo = level2;
                level++;
            }
            else if (level == 3)
            {
                roomToGo = level3;
                level++;
            }
            else if (level == 4)
            {
                roomToGo = level4;
                level++;
            }
            else if (level == 5)
            {
                roomToGo = win;
            }
        }

        if (!startRoom)
            this.gameObject.SetActive(false);

        return roomToGo;        
    }

    
}
